/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Player = __webpack_require__(1);

var _Player2 = _interopRequireDefault(_Player);

var _Game = __webpack_require__(2);

var _Game2 = _interopRequireDefault(_Game);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//import PlayerManager from './game/modules/PlayerManager';
var socket = io();
var ENTITIES = {};
var keys = { up: false, down: false, left: false, right: false };

socket.on("message", function (data) {
  console.log(data);
});

var game = new _Game2.default();
window.onresize = game.resize();

socket.on('entities', function (players) {
  for (var key in players) {
    if (ENTITIES[key]) {
      ENTITIES[key].x = players[key].x;
      ENTITIES[key].y = players[key].y;
      ENTITIES[key].texture.src = players[key].texture;
    } else {
      ENTITIES[key] = new _Player2.default(players[key].x, players[key].y, 50, 50, players[key].texture, game, players[key].id, "Giocatore..");
    }
  }
});

socket.on('remove_entity', function (id) {
  ENTITIES[id].remove();
  delete ENTITIES[id];
});

window.onkeyup = function (e) {
  var key = e.keyCode ? e.keyCode : e.which;

  if (key == 87) {
    keys.up = false;
  } else if (key == 83) {
    keys.down = false;
  } else if (key == 65) {
    keys.left = false;
  } else if (key == 68) {
    keys.right = false;
  }

  socket.emit('key_update', keys);
};

window.onkeydown = function (e) {
  var key = e.keyCode ? e.keyCode : e.which;
  // 38 : arrow up , 40: arrow down, 37: arrow left, 39: arrow right
  // 87: W key, 65: A key, 68: D key, 83: S key
  if (key == 87) {
    keys.up = true;
  } else if (key == 83) {
    keys.down = true;
  } else if (key == 65) {
    keys.left = true;
  } else if (key == 68) {
    keys.right = true;
  }

  console.log(keys);

  socket.emit('key_update', keys);
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Player = function () {
	function Player(x, y, w, h, texture, game, id, name) {
		_classCallCheck(this, Player);

		this.id = id; // ID mumber matching player socket id (planning to change format)
		this.name = name; // The name, for now it's a standard "Player" (Giocatore)
		this.ctx = game.playerManager.ctx; // Passing canvas context from this playermanager's ctx
		this.x = x; // x position on canvas
		this.y = y; // y position on canvas
		/* Rectangular texture */
		this.texture = new Image();
		this.texture.src = texture;
		this.ctx.scale(2, 2);

		/* Rectangular size */
		this.width = w;
		this.height = h;

		/* playerManager manager */
		this.playerManager = game.playerManager;
		this.playerManager.add(this);
	} // end constructor

	_createClass(Player, [{
		key: "remove",
		value: function remove() {
			this.playerManager.remove(this);
		}
	}, {
		key: "draw",
		value: function draw() {
			this.ctx.save();
			this.ctx.translate(this.x + this.width / 2, this.y + this.height / 2);
			this.ctx.translate(-this.width / 2, -this.height / 2);
			this.ctx.drawImage(this.texture, this.width, this.height);
			this.ctx.restore();
		}
	}]);

	return Player;
}(); // end Game class


exports.default = Player;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _PlayerManager = __webpack_require__(3);

var _PlayerManager2 = _interopRequireDefault(_PlayerManager);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Game = function () {
	function Game() {
		_classCallCheck(this, Game);

		this.canvas = document.getElementById("gameCanvas");
		this.ctx = this.canvas.getContext("2d");
		// Set double scale for pixel art visibility - TEMPORARY
		this.w = window.innerWidth;
		this.h = window.innerHeight;
		this.frames = 60;
		this.resize();
		this.players = {};
		// For each player connecting to the game, create a player manager which will be able to add or remove it
		this.playerManager = new _PlayerManager2.default(this.ctx);
		// This is the loop that will redraw players each frame (in case of new or removed ones)
		setInterval(function () {
			this.draw();
		}.bind(this), 1000 / this.frames);
	} // end constructor


	_createClass(Game, [{
		key: "fill",
		value: function fill() {
			// TEMPORARY!! DELETE IN PRODUCTION OR AFTER LOOP
			this.ctx.fillStyle = 'green';
			this.ctx.fillRect(10, 10, 100, 100);
			// save canvas image as data url (png format by default)
		} // end fill

	}, {
		key: "resize",
		value: function resize() {
			// Resize the canvas on window resize
			this.canvas.width = window.innerWidth;
			this.canvas.height = window.innerHeight;
		} // end resize

		// 1 frame - temporary

	}, {
		key: "draw",
		value: function draw() {
			// Call the actual drawing of players (calling playerManager's)
			// Clean the canvas
			this.ctx.clearRect(0, 0, this.w, this.h);
			// Delete sprite default smoothing because pixel art 
			this.ctx.webkitImageSmoothingEnabled = false;
			this.ctx.msImageSmoothingEnabled = false;
			this.ctx.imageSmoothingEnabled = false;
			// Call rendering of players
			this.playerManager.draw();
		}
	}]);

	return Game;
}(); // end Game class


exports.default = Game;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
   value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PlayerManager = function () {
   function PlayerManager(ctx) {
      _classCallCheck(this, PlayerManager);

      /* Canvas */
      this.ctx = ctx;
      /* Entitites */
      this.entities = [];
   } // end constructor

   _createClass(PlayerManager, [{
      key: "add",
      value: function add(entity) {
         this.entities.push(entity);
      }
   }, {
      key: "remove",
      value: function remove(entity) {
         this.entities.splice(this.entities.indexOf(entity), 1);
      } // end add

   }, {
      key: "draw",
      value: function draw() {
         for (var i = 0; i < this.entities.length; i++) {
            this.entities[i].draw();
            //console.log("There are " + this.entities.length + " entities...");
         }
      } // end draw

   }]);

   return PlayerManager;
}(); // end playerManager class


exports.default = PlayerManager;

/***/ })
/******/ ]);