# Farmer (ho bisogno di un nome più bello, ma arriverà con il resto del gioco)

Progetto di gioco multiplayer sul browser, ancora all'inizio, userò questo readme per tenere traccia di alcune cose importanti

### Pacchetti utilizzati

- Express
- Socket.io
- VueJS
- Gulp
- Webpack
- Babel (ES6 -> ES2015 comprensibile da tutti i browser)

### Da fare

- ~~Struttura Server\Client~~
- ~~Loop di gioco principale (Classe Game)~~
- ~~Creare la classe giocatore con tutte le informazioni inerenti~~
- ~~Scaling~~
- Configurare Sprites per le immagini
- Configurare la mappa di gioco (da decidere se 2 canvas con z-index diverse o un canvas solo, difficile.)
- Creazione instanze per le lobby
- Trovare un pixel artist perchè in pixel art faccio vomitare
- continua...

### Stato attuale - Connessione, Rendering Sprite giocatore
#### Movimento query\feedback server\client funzionante

![](farmer.webm)
