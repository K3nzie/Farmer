var gulp = require("gulp");
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var cssimport = require("gulp-cssimport");
var notify = require('gulp-notify');
//const babel = require('gulp-babel');
var webpack = require('webpack');
var webpackStream = require('webpack-stream');

var paths = {
	srcFolder: "src",
	buildFolder: "build",
	buildCss: 'build/assets/css',
	buildJs: 'build/assets/js',
	srcJsGame: "src/game/**/*.js",
	buildJsGame: "build/game",
	srcCss: "src/scss/**/*.{scss,sass}",
	srcClient: "src/client.js"

}


gulp.task('webpack', function() {
    return gulp.src(paths.srcClient)
        .pipe(webpackStream(require('./webpack.config.js'), webpack))
        .pipe(gulp.dest(paths.buildJs));

});
gulp.task('compileScss', function(){
	return gulp.src(paths.srcCss)
	.pipe(sass().on('error', sass.logError))
  .pipe(cleanCSS(function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
  .pipe(concat('styles.min.css'))
	.pipe(gulp.dest(paths.buildCss));
});

gulp.task('compileClient', function() {
    return gulp.src("src/client.js")
        //.pipe(concat('scripts.js'))
        //.pipe(gulp.dest())
       /* .pipe(babel({
            presets: ['env']
        }))*/
        .pipe(rename('client.min.js'))
        //.pipe(uglify())
        .pipe(gulp.dest(paths.buildJs));
});

gulp.task('default', ['compileScss', 'webpack'], function() {
    console.log("Ti gulpo tutto!");
});