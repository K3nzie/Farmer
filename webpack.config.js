var UglifyJsPlugin = require('uglifyjs-webpack-plugin');
module.exports = {
    watch: false,
    entry: './src/client.js',
  
    output: {
        path: __dirname + '/build/assets/js',
        filename: 'bundle.js'
        //libraryTarget: "commonjs2"
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: '/node_modules/',
                query: {
                    cacheDirectory: true,
                    presets: ['babel-preset-env']
                }
            }
        ]
    },

    plugins: [
      /*  new UglifyJsPlugin({
        compress: {
            warnings: false
        }
    })

    */]
};