var express = require('express');
var path = require('path');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

let players = {};
let pl = [];

app.use(express.static(path.join(__dirname, 'build/assets')));
app.set('view engine', 'pug');

server.listen(3000, () => console.log('http://127.0.0.1:3000 Farmer server is running...'));


io.on('connection', function (socket) {
	newPlayer(socket.id);
	socket.emit('entities', players);

	socket.on('disconnect', function(){
  		removePlayer(socket.id);
	});

	socket.on('key_update', function(data){
    if(data.up){
      players[socket.id].dy = -1;
    } else {
      players[socket.id].dy = 0;
    }

    if(data.down){
      players[socket.id].dy = 1;
    } else if(!data.up){
      players[socket.id].dy = 0;
    }

    if(data.left){
      players[socket.id].dx = -1;
    } else {
      players[socket.id].dx = 0;
    }

    if(data.right){
      players[socket.id].dx = 1;
    } else if(!data.left){
      players[socket.id].dx = 0;
    }

	}); // end onkeyupdate
});

function tick() {
	for(var i in players) {
		if(players[i].dx == 0 && players[i].dy == 0) continue;
   			players[i].x += players[i].dx;
			players[i].y += players[i].dy;
	}

	io.emit('entities', players);
}

function removePlayer(id) {
	delete players[id];
	console.log("Utente scollegato! - ID :" + id);
  	console.log("Totale utenti collegati : " + Object.keys(players).length);
  	io.emit('remove_entity', id);
}

function newPlayer(id) {
	console.log("Utente collegato! ID : " + id)
	io.emit('message', "Utente collegato!");
	players[id] = { 
		id: id,
		x: 300, 
		y: 300,
		dx: 0,
		dy: 0,
		//texture: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAqCAYAAABYzsDTAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjEwMPRyoQAAAUxJREFUWEftlk0KwjAQhb1CL9EjiD+4cyciint37goiiFAE1y506xkEV56gB/AC4jXcuIq8YmRME5MJqSJUeBT/vnnzOmlSq/3yNes3BDQfNHMthq1cy1H7TV4eK/hbbEmvLiDXWNJxR0BO2ZcKVxzAkU1OptUfvaD340qI07ogfO5Fpo6/Bu92NwJCJ7h6OZcxoAPqPAgcwPN2muceHH47pEIqOPy6T7RwOjXe01LBC6Max7GA5EKS73FF5uQ73phTkA5OC0dRxFtEX4F/ci1juewmPOd02aMA7YRGAjALLsGmmxkETh+3NufO7l0jkfMHcJZluawzyYUDCLA1f07eqktnuEve1ggKu/FzA3bY7e35WqrL54rpRMA2T/9QKhyF1AKmguwu1FOWrhgbSiG6Y5wXUJd3qfBgUZjaDXYTdQX+H15KB7p5t47jAzRfydFOtcn9AAAAAElFTkSuQmCC'
		texture: 'ch.png'
	};
	console.log("Totale utenti collegati : " + Object.keys(players).length);

}



// Routes

app.get('/', function (req, res) {
  res.render('index');
});

app.get('/game', function (req, res) {
  res.render('game');
});

setInterval(tick, 1000/60);