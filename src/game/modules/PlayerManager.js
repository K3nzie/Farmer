export default class PlayerManager {

	constructor(ctx){
    /* Canvas */
    this.ctx = ctx;
    /* Entitites */
    this.entities = [];

	} // end constructor

	add(entity){
    this.entities.push(entity);
	}
	remove(entity){
    this.entities.splice(this.entities.indexOf(entity), 1);
	} // end add

  	draw(){
    	for(let i = 0; i < this.entities.length; i++){
    		this.entities[i].draw();
    		//console.log("There are " + this.entities.length + " entities...");
    	}
	} // end draw

} // end playerManager class