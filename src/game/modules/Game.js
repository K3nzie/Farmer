import PlayerManager from './PlayerManager';
export default class Game {
	constructor() {
		this.canvas = document.getElementById("gameCanvas");
		this.ctx = this.canvas.getContext("2d");
		// Set double scale for pixel art visibility - TEMPORARY
		this.w = window.innerWidth;
		this.h = window.innerHeight;
		this.frames = 60; 
		this.resize();
		this.players = {};
		// For each player connecting to the game, create a player manager which will be able to add or remove it
		this.playerManager = new PlayerManager(this.ctx);
		// This is the loop that will redraw players each frame (in case of new or removed ones)
		setInterval(function(){
      		this.draw();
		}.bind(this), 1000 / this.frames); 

		
	} // end constructor


	fill() { // TEMPORARY!! DELETE IN PRODUCTION OR AFTER LOOP
		this.ctx.fillStyle = 'green';
		this.ctx.fillRect(10, 10, 100, 100);
		// save canvas image as data url (png format by default)
	} // end fill

	resize() {
		// Resize the canvas on window resize
		this.canvas.width = window.innerWidth;
		this.canvas.height = window.innerHeight;
	} // end resize

	// 1 frame - temporary
	draw() { // Call the actual drawing of players (calling playerManager's)
		// Clean the canvas
		this.ctx.clearRect(0, 0, this.w, this.h);
		// Delete sprite default smoothing because pixel art 
		this.ctx.webkitImageSmoothingEnabled = false;
 		this.ctx.msImageSmoothingEnabled = false;
 		this.ctx.imageSmoothingEnabled = false;
 		// Call rendering of players
		this.playerManager.draw();
	}
} // end Game class

