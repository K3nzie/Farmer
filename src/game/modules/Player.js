export default class Player {
	constructor(x, y, w, h, texture, game, id, name) {
		this.id = id; // ID mumber matching player socket id (planning to change format)
		this.name = name; // The name, for now it's a standard "Player" (Giocatore)
		this.ctx = game.playerManager.ctx; // Passing canvas context from this playermanager's ctx
		this.x = x; // x position on canvas
		this.y = y; // y position on canvas
		/* Rectangular texture */
    	this.texture = new Image();
    	this.texture.src = texture;
    	this.ctx.scale(2, 2); // Scaling, probably temporary

    	/* Rectangular size */
    	this.width = w;
		this.height = h;

		/* playerManager manager */
		this.playerManager = game.playerManager;
		this.playerManager.add(this);

		
	} // end constructor
	
	remove() {	
    	this.playerManager.remove(this);
	}

	draw() {
		this.ctx.save();
		this.ctx.translate(this.x + this.width / 2, this.y + this.height / 2);
		this.ctx.translate(-this.width / 2, -this.height / 2);
		this.ctx.drawImage(this.texture, this.width, this.height);
		this.ctx.restore();


	}

} // end Game class