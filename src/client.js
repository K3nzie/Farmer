//import PlayerManager from './game/modules/PlayerManager';
import Player from './game/modules/Player';
import Game from './game/modules/Game';

let socket = io();
let ENTITIES = {};
let keys = { up : false, down : false, left : false, right : false };


socket.on("message", function(data) {
	console.log(data);
});


let game = new Game();
window.onresize = game.resize();

socket.on('entities', function(players){
    for(var key in players){
      if(ENTITIES[key]){
        ENTITIES[key].x = players[key].x;
        ENTITIES[key].y = players[key].y;
        ENTITIES[key].texture.src = players[key].texture;
      } else {
        ENTITIES[key] = new Player(players[key].x, players[key].y, 50, 50, players[key].texture, game, players[key].id, "Giocatore..");
      }
    }
});

socket.on('remove_entity', function(id){
    ENTITIES[id].remove();
    delete ENTITIES[id];
});


window.onkeyup = function(e) {
    let key = e.keyCode ? e.keyCode : e.which;
  
    if(key == 87){
      keys.up = false;
    } else if(key == 83){
      keys.down = false;
    } else if(key == 65){
      keys.left = false;
    } else if(key == 68){
      keys.right = false;
    }
  
    socket.emit('key_update', keys);
  }
  
  window.onkeydown = function(e) {
    let key = e.keyCode ? e.keyCode : e.which;
    // 38 : arrow up , 40: arrow down, 37: arrow left, 39: arrow right
    // 87: W key, 65: A key, 68: D key, 83: S key
    if(key == 87){
      keys.up = true;
    } else if(key == 83){
      keys.down = true;
    } else if(key == 65){
      keys.left = true;
    } else if(key == 68){
      keys.right = true;
    }
  
    console.log(keys);
  
    socket.emit('key_update', keys);
}